use std::sync::mpsc::sync_channel;
use std::sync::Mutex;
use std::thread;
use std::thread::JoinHandle;
use std::time::Duration;
use crate::concurrent::cyclic_barrier::CyclicBarrier;
use crate::concurrent::exchanger;
use crate::concurrent::semaphore::Semaphore;
use crate::concurrent::sharedhashmap::{Command, SharedHashMap};
use crate::concurrent::synchronizer::Synchronizer;

mod concurrent {
    pub mod count_down_latch {
        use std::sync::{Condvar, Mutex};

        pub struct CountDownLatch {
            count: Mutex<usize>,
            cond: Condvar
        }

        impl CountDownLatch {
            pub fn new(count: usize) -> Self {
                CountDownLatch { count: Mutex::new(count), cond: Condvar::new()}
            }

            pub fn wait(&self) {
                let mut count = self.count.lock().unwrap();
                while *count > 0 {
                    count = self.cond.wait(count).unwrap();
                }
            }

            pub fn countdown(&self) {
                let mut count = self.count.lock().unwrap();
                *count -= 1;
                if *count == 0 {
                    self.cond.notify_all();
                }
            }
        }
    }
    pub mod execution_limiter {
        use std::sync::{Arc, Condvar, Mutex};

        struct Dropper<'a> { element: &'a ExecutionLimiter}

        impl<'a> Drop for Dropper<'a> {
            fn drop(&mut self) {
                self.element.decrement();
            }
        }

        pub struct ExecutionLimiter {
            mutex: Mutex<usize>,
            max: usize,
            cond: Condvar
        }

        impl ExecutionLimiter {
            pub fn new(n: usize) -> Arc<Self> {
                Arc::new(
                    ExecutionLimiter{
                        mutex: Mutex::new(0),
                        max: n,
                        cond: Condvar::new()
                    }
                )
            }
            // Può essere un modo per passare in maniera semplice
            // una funzione senza usare Box<dyn Trait FnOnce>
            pub fn execute<R>(&self, f: impl FnOnce() -> R) -> R {
                let mut n = self.mutex.lock().unwrap();
                while *n == self.max {
                    n = self.cond.wait(n).unwrap();
                }
                *n = *n + 1;
                drop(n);
                // Applico il paradigma RAII, in modo che al drop di _d
                // venga effettuata correttamente una azione.
                let _d = Dropper{ element: self};
                f()
            }

            fn decrement(&self) {
                *self.mutex.lock().unwrap() -= 1;
                self.cond.notify_one();
            }
        }
    }
    pub mod packaged_task {
        use std::fmt::{Display, Formatter};
        use std::error::Error;
        use std::panic::UnwindSafe;
        use std::sync::mpsc::{channel, Receiver, TryRecvError};
        use crate::concurrent::packaged_task::AsyncResult::{Done, OnGoing};

        #[derive(Debug)]
        pub struct PTError {
            message: String
        }
        pub enum AsyncResult<A, B> {
            OnGoing(A),
            Done(B)
        }

        impl PTError {
            fn new(msg: &str) -> Self {
                PTError {message: msg.to_string()}
            }
        }

        impl Display for PTError {
            fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
                write!(f, "PTError: {}", self.message)
            }
        }

        impl Error for PTError {}

        type Result<T> = std::result::Result<T, PTError>;

        pub struct Future<T: Send> {
            rx: Receiver<Result<T>>
        }

        impl<T: Send> Future<T> {
            fn new(rx: Receiver<Result<T>>) -> Self {
                Future { rx }
            }

            pub fn get(self) -> Result<T> {
                let r = self.rx.recv();
                if r.is_err() { Err(PTError::new("Packaged task has been dropped")) }
                else { r.unwrap() }
            }

            pub fn try_get(self) -> AsyncResult<Self, Result<T>> {
                let r = self.rx.try_recv();
                if r.is_err() {
                    match r.err().unwrap() {
                        TryRecvError::Empty => return OnGoing(self),
                        TryRecvError::Disconnected => return Done(Err(PTError::new("Packaged task has been dropped")))
                    };
                } else {
                    return Done(r.unwrap())
                }
            }
        }

        pub struct PackagedTask<Args> {
            f: Box<dyn FnOnce(Args)>
        }

        // Ci prendiamo la responsabilità
        unsafe impl<Args> Send for PackagedTask<Args>{}

        impl<Args> PackagedTask<Args>{
            fn new(f: Box<dyn FnOnce(Args)>) -> Self {
                PackagedTask{ f }
            }

            pub fn execute(self, a: Args) {
                (self.f)(a);
            }
        }

        pub fn packaged_task<F, Args, T: 'static + Send>(f: F) -> (PackagedTask<Args>, Future<T>)
        where
            F: FnOnce(Args) -> T + UnwindSafe + 'static, // UnwindSafe è un tratto particolare, serve per evitare che nel caso F panichi, e in quel caso bisogna bloccare il thread a metà strada. Non vogliamo rompere tutto, per blocchi safe è garantita.
                                                        // F non deve cambiare nel tempo, non deve essere mutabile. In questo modo siamo sicuri.
            Args: UnwindSafe, // Args non deve fare "maialate" sullo stack
        {
            let (tx, rx) = channel::<Result<T>>();
            let g = move |a: Args| {
                // Se f può rompersi, la chiamiamo attraverso un wrapper
                // se la funzione panica, non fa panicare l'intero stack, ma ritorna un Err. E' un modo sicuro
                let res = std::panic::catch_unwind(||f(a))
                    .map_err(|_|PTError::new("Lambda panicked"));
                tx.send(res).unwrap_or(());
            };

            let pt = PackagedTask::new(Box::new(g));
            let future = Future::new(rx);
            (pt, future)
        }
    }
    pub mod exchanger {
        use std::sync::{Arc, Condvar, Mutex};

        pub(crate) struct Exchanger<T: Send + Clone + Default> {
            state: Mutex<Status>,
            cv: Condvar,
            val1: Mutex<T>,
            val2: Mutex<T>,
        }

        #[derive(PartialEq)]
        enum Status {
            Created,
            Filling,
            Full
        }

        impl<T: Send + Clone + Default> Exchanger<T>{
            pub fn new() -> Arc<Exchanger<T>> {
                Arc::new(Exchanger{state: Mutex::new(Status::Created), cv: Condvar::new(), val1: Mutex::new(T::default()), val2: Mutex::new(T::default()) })
            }

            pub fn exchange(&self, val: T) -> T {
                let mut state = self.state.lock().unwrap();
                match *state {
                    Status::Created => {
                        let mut v1 = self.val1.lock().unwrap();
                        *v1 = val;
                        drop(v1);
                        *state = Status::Filling;
                        while *state != Status::Full {
                            state = self.cv.wait(state).unwrap()
                        }
                        return self.val2.lock().unwrap().clone();
                    },
                    Status::Filling => {
                        *state = Status::Full;
                        let mut v2 = self.val2.lock().unwrap();
                        *v2 = val;
                        drop(v2);
                        self.cv.notify_one();
                        return self.val1.lock().unwrap().clone();

                    }
                    _ => { panic!("Oh, no!")}
                }
            }
        }
    }
    pub mod looper {
        use std::sync::mpsc::{channel, Sender};
        use std::thread;

        #[derive(Debug, Clone)]
        pub struct Looper<M: Send + 'static>{
            tx: Sender<M>
        }

        impl<M: Send + 'static> Looper<M> {
            pub fn new(process: impl Fn(M) ->() + Send + 'static, cleanup: impl FnOnce() -> () + Send + 'static) -> Self {
                let (tx, rx) = channel::<M>();
                thread::spawn(move ||{
                    while let Ok(msg) = rx.recv() {
                        process(msg);
                    }
                    cleanup();
                });
                Looper{tx }
            }
            pub fn send(&self, msg: M) {
                self.tx.send(msg).unwrap();
            }
        }

    }
    pub mod single_thread_executor {
        use std::error::Error;
        use std::fmt::{Display, Formatter};
        use std::sync::mpsc::{channel, Sender};
        use std::sync::Mutex;
        use std::thread;
        use std::thread::JoinHandle;

        #[derive(Debug, Clone)]
        pub struct ExecutorError {
            msg: String,
        }

        impl ExecutorError {
            pub fn new(msg: &str) -> Self {
                ExecutorError{msg: msg.to_string()}
            }
        }

        impl Display for ExecutorError {
            fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
                write!(f, "{}", self.msg)
            }
        }

        impl Error for ExecutorError {}

        enum State {
            Stop,
            Run
        }

        pub struct SingleThreadExecutor {
            tx: Sender<Box<dyn Fn() + Send + 'static>>,
            thread: JoinHandle<()>,
            state: Mutex<State>,
        }

        impl SingleThreadExecutor {
            pub fn new() -> Self {
                let (tx, rx) = channel::<Box<dyn Fn() + Send + 'static>>();
                let thread = thread::spawn(move || {
                    while let Ok(task) = rx.recv() {
                        (*task)();
                    }
                });
                SingleThreadExecutor{ tx, thread, state: Mutex::new(State::Run) }
            }

            pub fn submit(&self, f: Box<dyn Fn() + Send + 'static>) -> Result<(), ExecutorError> {
                match *self.state.lock().unwrap() {
                    State::Run => { self.tx.send(f).unwrap(); Ok(()) },
                    State::Stop => Err(ExecutorError::new("Single thread executor closed.")),
                }
            }

            pub fn close(&self) {
                *self.state.lock().unwrap() = State::Stop;
            }

            pub fn join(self) {
                self.close();
                self.thread.join().unwrap();
            }
        }


    }
    pub mod cyclic_barrier {
        use std::sync::{Arc, Condvar, Mutex};
        use crate::concurrent::cyclic_barrier::State::{Entering, Exiting};

        #[derive(PartialEq)]
        pub enum State {
            Entering(usize),
            Exiting(usize),
        }

        pub struct CyclicBarrier {
            state: Mutex<State>,
            cv: Condvar,
            max: usize
        }

        impl CyclicBarrier {
            pub fn new(n: usize) -> Arc<Self> {
                Arc::new(
                    CyclicBarrier {
                    state: Mutex::new(State::Entering(0)),
                    cv: Condvar::new(),
                    max: n
                    }
                )
            }

            fn is_entering(state: &State) -> bool {
                match *state {
                    Entering(_) => true,
                    _ => false
                }
            }

            pub fn wait(&self) {
                let mut status = self.state.lock().unwrap();
                // Aspetto, il mutex sta svuotando se stesso
                status = self.cv.wait_while(status, |status| !Self::is_entering(status)).unwrap();

                if let Entering(current_val) = *status {
                    // Il mutex si trova nello stadio Entering. Io potrei essere l'ultimo,
                    // in tal caso procedere a cambiare stato.
                    if current_val == self.max - 1 {
                        *status = Exiting(current_val);
                        self.cv.notify_all();
                    } else {
                        // Non sono l'ultimo, quindi aggiorno lo stato e mi metto in attesa
                        *status = Entering(current_val + 1);
                        // Attendo fino a quando lo stato rimane entering
                        status = self.cv.wait_while(status, |status| Self::is_entering(status)).unwrap();
                        // Qualcuno ha cambiato lo stato in Exiting, esco
                        if let Exiting(v) = *status {
                            if v == 1 {
                                // Sono l'ultimo ad uscire, metto lo stato a Entering di 0
                                *status = Entering(0);
                                // sveglio i thread che stanno aspettando per entrare.
                                self.cv.notify_all();
                            } else {
                                *status = Exiting(v - 1);
                            }
                        }
                    }
                }

            }
        }
    }
    pub mod joiner {
        use std::collections::HashMap;
        use std::sync::{Arc, Condvar, Mutex};
        use crate::concurrent::joiner::State::{Entering, Exiting};


        pub enum State {
            Entering(usize),
            Exiting(usize),
        }

        pub struct Joiner {
            max: usize,
            hm: Mutex<HashMap<i32, f32>>,
            state: Mutex<State>,
            cv: Condvar
        }

        impl Joiner {
            pub fn new(max: usize) -> Arc<Self> {
                Arc::new(Joiner{max, hm: Mutex::new(HashMap::new()), state: Mutex::new(Entering(0)), cv: Condvar::new() })
            }

            pub fn is_entering(state: &State) -> bool {
                match *state {
                    Entering(_) => true,
                    _ => false
                }
            }

            pub fn supply(&self, key: i32, value: f32) -> HashMap<i32, f32> {
                let mut status = self.state.lock().unwrap();
                status = self.cv.wait_while(status, |s| !Self::is_entering(s)).unwrap();
                // Lo stato ora è entering, posso entrare e inserire la mia entry

                if let Entering(cur) = *status {
                    if cur == self.max - 1 {
                        {
                            (*self.hm.lock().unwrap()).insert(key, value);

                        }
                        *status = Exiting(cur);
                        // Inizio lo svuotamento
                        self.cv.notify_all();
                    } else {
                        // Entro e attendo che la coda si riempi
                        {
                            (*self.hm.lock().unwrap()).insert(key, value);

                        }
                        *status = Entering(cur + 1);
                        status = self.cv.wait_while(status, |s| Self::is_entering(s)).unwrap();
                        if let Exiting(cur) = *status {
                            if cur == 1 {
                                //Sono l'ultimo, devo far ripartire il conto
                                let res = self.hm.lock().unwrap().clone();
                                self.hm.lock().unwrap().clear();
                                *status = Entering(0);
                                self.cv.notify_all();
                                return res;
                            } else {
                                *status = Exiting(cur - 1);
                                return self.hm.lock().unwrap().clone();
                            }
                        }
                        // Sto uscendo, devo ritornare la hashmap
                        return self.hm.lock().unwrap().clone();
                    }
                }
                HashMap::new()
            }
        }
    }
    /*pub mod executor {
        use std::sync::Arc;
        use std::sync::mpsc::{channel, Receiver, Sender};
        use std::thread;

        pub struct Future<T: Send> {
            rx: Receiver<T>
        }

        impl<T: Send> Future<T> {
            pub fn get(&self) -> T {
                self.rx.recv().unwrap()
            }
        }

        pub struct Executor<F: FnOnce() + Send + 'static> {
            tx: Sender<F>,
            rx: Receiver<()>
        }

        impl <F: FnOnce() + Send + 'static> Executor<F> {
            pub fn new() -> Self {
                let (tx, rx) = channel::<F>();
                let (res_tx, res_rx) = channel::<()>();
                let _ = thread::spawn(move||{
                   while let Ok(task) = rx.recv() {
                       task();
                       res_tx.send(()).unwrap();
                   }
                });
                Executor{tx, rx: res_rx}
            }

            pub fn submit(&self, f: F) -> Future<()>{
                self.tx.send(f).unwrap();
                Future{rx: *Arc::new(self.rx).clone() }
            }
        }
    }*/
    pub mod semaphore {
        use std::sync::{Arc, Condvar, Mutex};

        pub struct Semaphore {
            val: Mutex<usize>,
            cv: Condvar
        }

        impl Semaphore {
            pub fn new(n: usize) -> Arc<Self> {
                Arc::new(
                    Semaphore{val:Mutex::new(n), cv: Condvar::new()}
                )
            }

            pub fn wait(&self) {
                let mut val = self.val.lock().unwrap();
                val = self.cv.wait_while(val, |n| *n == 0).unwrap();
                *val -= 1
            }

            pub fn signal(&self) {
                *self.val.lock().unwrap() += 1;
                self.cv.notify_one();
            }
        }
    }
    pub mod synchronizer {
        use std::mem::take;
        use std::sync::{Arc, Condvar, Mutex};
        use std::sync::mpsc::{channel, Sender};
        use std::thread;
        use std::thread::JoinHandle;

        pub struct Synchronizer{
            state: Mutex<(Option<f32>, Option<f32>)>,
            cv: Condvar,
            tx: Mutex<Sender<Option<(f32, f32)>>>,
            thread: Option<JoinHandle<()>>
        }

        fn is_full(state: &(Option<f32>, Option<f32>)) -> bool{
            return state.0.is_some() && state.1.is_some();
        }

        fn is_empty(state: &(Option<f32>, Option<f32>)) -> bool{
            return state.0.is_none() && state.1.is_none();
        }

        impl Synchronizer{
            pub fn new(process: impl Fn(f32, f32) + Send + 'static) -> Arc<Self> {
                let (tx, rx) = channel::<Option<(f32, f32)>>();
                let thread = thread::spawn(move ||{
                    while let Ok(data) = rx.recv() {
                        match data {
                            Some(inner) => process(inner.0, inner.1),
                            None => break,
                        }
                    }
                });
                Arc::new(Synchronizer{state: Mutex::new((None, None)), cv: Condvar::new(), tx: Mutex::new(tx), thread: Some(thread)})
            }

            pub fn dataFromFirstPort(&self, d1: f32) {
                let mut state = self.state.lock().unwrap();
                (*state).0 = Some(d1);
                //println!("S1: current state: {:?}", state);
                state = self.cv.wait_while(state, |s| !is_full(s)).unwrap();
                //println!("S1: current state: {:?}", state);
                self.tx.lock().unwrap().send(Some(((*state).0.unwrap(), (*state).1.unwrap()))).unwrap();
                *state = (None, None);
                self.cv.notify_one();
            }

            pub fn dataFromSecondPort(&self, d2: f32) {
                let mut state = self.state.lock().unwrap();
                (*state).1 = Some(d2);
                //println!("S2: current state: {:?}", state);
                self.cv.notify_one();
                state = self.cv.wait_while(state,|s| !is_empty(s)).unwrap();
                //println!("S2: return, state: {:?}", state);
            }
        }

        impl Drop for Synchronizer {
            fn drop(&mut self) {
                self.tx.lock().unwrap().send(None).unwrap();
                take(&mut self.thread).unwrap().join().unwrap();
            }
        }
    }
    pub mod sharedhashmap {
        use std::collections::HashMap;
        use std::fmt::Debug;
        use std::hash::Hash;
        use std::mem::take;
        use std::sync::mpsc::{channel, Sender, Receiver};
        use std::sync::{Arc, Mutex};
        use std::thread;
        use std::thread::JoinHandle;

        #[derive(Debug)]
        pub enum Command<K: Send + 'static, V: Send + 'static> {
            Insert(K, V),
            Get(K),
            Clear,
            Store,
            Exit
        }

        #[derive(Debug)]
        pub enum HashmapResult<K: Debug + Send + 'static, V: Debug + Send + 'static> {
            Value(V),
            Res(bool),
            HashMap(HashMap<K, V>)
        }

        pub struct Future<T> {
            rx: Receiver<T>
        }

        impl<T> Future<T> {
            pub fn get(&self) -> T {
                self.rx.recv().unwrap()
            }
        }

        pub struct SharedHashMap<K: Send + 'static + Debug + Eq + Hash + Clone, V: Send + 'static + Debug + Copy> {
            hm: Arc<Mutex<HashMap<K, V>>>,
            tx: Mutex<Sender<Command<K, V>>>,
            handle: Option<JoinHandle<()>>,
        }

        impl<K: Send + 'static + Debug + Eq + Hash + Clone, V: Send + 'static + Debug + Copy> SharedHashMap<K, V> {
            pub fn new() -> (Arc<Self>, Future<HashmapResult<K, V>>) {
                let (tx, rx) = channel();
                let (result_tx, result_rx) = channel();
                let hm = Arc::new(Mutex::new(HashMap::new()));
                let local_map = hm.clone();
                let handle = thread::spawn(move ||{
                    while let Ok(command) = rx.recv() {
                        match command {
                            Command::Exit => break,
                            Command::Insert(key, value) => {
                                let mut local_map = local_map.lock().unwrap();
                                (*local_map).insert(key, value);
                                result_tx.send(HashmapResult::Res(true)).unwrap()
                            },
                            Command::Get(key) => {
                                let mut local_map = local_map.lock().unwrap();
                                result_tx.send(HashmapResult::Value(*((*local_map).get(&key).unwrap()))).unwrap();
                            },
                            Command::Clear => {
                                let mut local_map = local_map.lock().unwrap();
                                (*local_map).clear();
                                result_tx.send(HashmapResult::Res(true)).unwrap();
                            },
                            Command::Store => {
                                let mut local_map = local_map.lock().unwrap();
                                let res = (*local_map).clone();
                                result_tx.send(HashmapResult::HashMap(res)).unwrap();
                            }
                            _ => {
                                println!("Received command {:?}", command);
                                result_tx.send(HashmapResult::Res(true)).unwrap()
                            },
                        }
                    }
                });

                let shm: Arc<SharedHashMap<K, V>> = Arc::new(
                    SharedHashMap{
                        hm: hm,
                        tx: Mutex::new(tx),
                        handle: Some(handle),
                    });
                ( shm, Future{rx: result_rx})
            }

            pub fn send(&self, command: Command<K, V>){
                self.tx.lock().unwrap().send(command).unwrap();
            }
        }

        impl<K: Send + 'static + Debug + Eq + Hash + Clone, V: Send + 'static + Debug + Copy> Drop for SharedHashMap<K, V> {
            fn drop(&mut self) {
                // Causa l'uscita
                self.tx.lock().unwrap().send(Command::Exit).unwrap();
                take(&mut self.handle).unwrap().join().unwrap();
            }
        }
    }
}

fn main() {
    let (tx, rx) = sync_channel::<()>(0);
    let t1 = thread::spawn(move||{
        for i in 1..10 {
            tx.send(()).unwrap();
            println!("T1: {}", i);
            thread::sleep(Duration::from_nanos(1));
        }
    });
    let t2 = thread::spawn(move||{
        for i in 1..10 {
            rx.recv().unwrap();
            println!("T2: {}", i);
            thread::sleep(Duration::from_nanos(1))
        }
    });

    t1.join().unwrap();
    t2.join().unwrap();

}
